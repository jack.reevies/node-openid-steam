const { get, post, CONTENT_TYPES } = require('@spacepumpkin/minimal-request')
const RSA = require('node-bignumber').Key
const hex2b64 = require('node-bignumber').hex2b64
const SteamTotp = require('steam-totp')

function wait (ms) { return new Promise(resolve => setTimeout(resolve, ms)) }

/**
 * @param {String|{sessionid, steamLogin, steamLoginSecure}} username Account Name if logging into steam or an object of logged in cookies
 * @param {String} password Account password if logging into steam, else null
 * @param {String} sharedSecret Shared Secret (part of 2FA) if logging into steam, else null
 */
async function getSTNHash (username, password, sharedSecret) {
  return authenticateWithOpenId(username, password, sharedSecret, 'https://stntrading.eu/login', 'stn_hash')
}

/**
 * @param {String|{sessionid, steamLogin, steamLoginSecure}} username Account Name if logging into steam or an object of logged in cookies
 * @param {String} password Account password if logging into steam, else null
 * @param {String} sharedSecret Shared Secret (part of 2FA) if logging into steam, else null
 */
async function getScrapTFHash (username, password, sharedSecret) {
  return authenticateWithOpenId(username, password, sharedSecret, 'https://scrap.tf/login', 'scr_session')
}

/**
 * @param {String|{sessionid, steamLogin, steamLoginSecure}} username Account Name if logging into steam or an object of logged in cookies
 * @param {String} password Account password if logging into steam, else null
 * @param {String} sharedSecret Shared Secret (part of 2FA) if logging into steam, else null
 * @param {String} loginUrl The login Url of the site (ie, scrap.tf/login)
 * @param {String} targetCookie The name of the cookie the target site uses to store the session has (ie, scr_session for ScrapTF)
 */
async function authenticateWithOpenId (username, password, sharedSecret, loginUrl, targetCookie) {
  const { cookies } = typeof (username) === 'string' ? await getSessionCookiesForSteam(username, password, sharedSecret) : { cookies: username }
  let redirectUrl = await getOpenIdAuthRedirectUrlFromSteamCookies(cookies, loginUrl)
  let siteResponse = null
  while (true) {
    siteResponse = await get(redirectUrl)
    if (siteResponse.headers.location && siteResponse.headers.location.startsWith('http')) {
      redirectUrl = siteResponse.headers.location
    } else break
  }
  const cookiesHash = siteResponse.headers['set-cookie'].find(o => o.startsWith(`${targetCookie}=`))
  if (!cookiesHash) throw new Error(`Site did not reply with a ${targetCookie} cookie (GET ${redirectUrl} responded with ${siteResponse.statusCode})`)
  const expiry = Date.now() + (Number(/Max-Age=(\d+)/gm.exec(cookiesHash)[1]) * 1000)
  return { hash: siteResponse.cookies[targetCookie], expires: expiry }
}

async function getSessionCookiesForSteam (username, password, sharedSecret, emailAuthCode) {
  const dateNow = Date.now()
  const rsaKey = await getRsaKey(username, dateNow)
  const { mod, exp, timestamp } = rsaKey
  const b64EncPassword = getB64EncPassword(mod, exp, password)
  let steamDoLoginResponse
  while (true) {
    const authCode = sharedSecret ? SteamTotp.generateAuthCode(sharedSecret) : undefined
    steamDoLoginResponse = await steamDoLogin(dateNow, username, b64EncPassword, authCode, emailAuthCode, timestamp)
    if (steamDoLoginResponse.body.requires_twofactor) {
      await wait(30000)
    } else {
      break
    }
  }
  if (!steamDoLoginResponse.body.success) {
    if (steamDoLoginResponse.body.captcha_needed) {
      throw new Error('CAPTCHA needed')
    } else {
      throw new Error(`Failed to login. ${steamDoLoginResponse.body.message}`)
    }
  }
  return { cookies: steamDoLoginResponse.cookies, steam_response: steamDoLoginResponse.body }
}

async function getNonce (redirectUrl) {
  const response = await get(redirectUrl)
  let raw = response.body
  raw = raw.substring(raw.indexOf('<form enctype="multipart/form-data" action="https://steamcommunity.com/openid/login" method="POST" name="loginForm" id="openidForm">'))
  const jwtStart = '<input type="hidden" name="openidparams" value="'
  let jwt = raw.substring(raw.indexOf(jwtStart) + jwtStart.length)
  jwt = jwt.substring(0, jwt.indexOf('" />'))
  const nonceStart = 'name="nonce" value="'
  let nonce = raw.substring(raw.indexOf(nonceStart) + nonceStart.length)
  nonce = nonce.substring(0, nonce.indexOf('" />'))
  return { response: response, jwt: jwt, nonce: nonce }
}

function getB64EncPassword (mod, exp, password) {
  var key = new RSA()
  key.setPublic(mod, exp)

  const enc = hex2b64(key.encrypt(password))
  return enc
}

async function getRsaKey (username, donotcache) {
  const body = {
    donotcache: donotcache,
    username: username
  }

  const response = await post('https://steamcommunity.com/login/getrsakey/', { body, contentType: CONTENT_TYPES.FORM_URL_ENCODED })
  const { publickey_mod: mod, publickey_exp: exp, timestamp } = response.body
  if (!mod || !exp || !timestamp) { throw new Error('Steam returned fail when getting RSAKey') }
  return { mod, exp, timestamp }
}

async function getOpenIdAuthRedirectUrlFromSteamCookies (cookies, loginUrl) {
  let openIdLoginUrl = loginUrl
  if (!loginUrl.startsWith('https://steamcommunity.com/openid/login')) {
    const thirdPartyLoginResponse = await get(loginUrl)
    if (!thirdPartyLoginResponse.headers.location) { throw new Error("Expected 'loginUrl' to redirect us") }
    openIdLoginUrl = thirdPartyLoginResponse.headers.location
  }

  const { response, nonce, jwt } = await getNonce(openIdLoginUrl)
  cookies = { ...response.cookies, ...cookies }
  const openIdLoginResponse = await openIdLogin(jwt, nonce, cookies)
  if (!openIdLoginResponse.headers.location || openIdLoginResponse.headers.location.startsWith('https://steamcommunity.com')) throw new Error('Steam OpenID Login Failed (response redirected to homepage)')
  return openIdLoginResponse.headers.location
}

async function getOpenIdAuthRedirectUrl (username, password, sharedSecret, loginUrl) {
  const { cookies } = await getSessionCookiesForSteam(username, password, sharedSecret)
  return getOpenIdAuthRedirectUrlFromSteamCookies(cookies, loginUrl)
}

async function steamDoLogin (donotcache, username, b64EncPassword, authCode, emailAuth, rsaTimestamp) {
  const body = {
    donotcache: donotcache,
    password: b64EncPassword,
    username: username,
    twofactorcode: authCode,
    emailauth: emailAuth,
    loginfriendlyname: undefined,
    captchagid: -1,
    captcha_text: undefined,
    emailsteamid: undefined,
    rsatimestamp: rsaTimestamp,
    remember_login: true
  }
  const headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0',
    Accept: '*/*',
    'Accept-Language': 'en-US,en;q=0.5',
    // 'Referer': 'https://steamcommunity.com/openid/login?openid.ns=http://specs.openid.net/auth/2.0&openid.mode=checkid_setup&openid.return_to=https://stntrading.eu/login&openid.realm=https://stntrading.eu&openid.ns.sreg=http://openid.net/extensions/sreg/1.1&openid.claimed_id=http://specs.openid.net/auth/2.0/identifier_select&openid.identity=http://specs.openid.net/auth/2.0/identifier_select',
    'X-Requested-With': 'XMLHttpRequest',
    DNT: '1',
    Connection: 'keep-alive',
    Cookie: 'steamCountry=GB%7C1b3cff7302435527c3c54c452fa512d2'
  }

  const response = await post('https://steamcommunity.com/login/dologin/', { headers, body })
  return response
}

async function openIdLogin (jwt, nonce, cookies) {
  const body = {
    action: 'steam_openid_login',
    'openid.mode': 'checkid_setup',
    openidparams: jwt,
    nonce: nonce
  }

  const response = await post('https://steamcommunity.com/openid/login', { body, cookies, contentType: CONTENT_TYPES.MULTIPART_FORM_DATA })
  return response
}

module.exports = {
  getScrapTFHash,
  getSTNHash,
  getSessionCookiesForSteam,
  getOpenIdAuthRedirectUrlFromSteamCookies,
  authenticateWithOpenId,
  getOpenIdAuthRedirectUrl
}
